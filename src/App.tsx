import * as React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "./App.css";

import Home from "./containers/Home";
import CreatePage from "./containers/Create";
import Detail from "./containers/Detail";

class App extends React.Component<{}> {
  public render() {
    return (
      <Router>
        <div>
          <Route exact path="/" component={Home} />
          <Route path="/detail/:id" component={Detail} />
          <Route path="/create" component={CreatePage} />
        </div>
      </Router>
    );
  }
}

export default App;
