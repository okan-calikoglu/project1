export interface CreateItemState {
  write?: string;
  title?: string;
  publisher?: string;
}

export interface CreateItemProps {
  postItem: any;
}
