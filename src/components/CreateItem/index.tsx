import * as React from "react";

import { Input } from "../Input";
import { CreateItemState, CreateItemProps } from "./interface";

export default class CreateItem extends React.Component<
  CreateItemProps,
  CreateItemState
> {
  constructor(props: any) {
    super(props);
    this.state = {
      write: "",
      title: "",
      publisher: ""
    };
    this.writeValue = this.writeValue.bind(this);
    this.titleValue = this.titleValue.bind(this);
    this.publisherValue = this.publisherValue.bind(this);
  }

  writeValue(event: any) {
    this.setState({ write: event.target.value });
  }
  titleValue(event: any) {
    this.setState({ title: event.target.value });
  }
  publisherValue(event: any) {
    this.setState({ publisher: event.target.value });
  }

  public render() {
    return (
      <div className="create">
        <Input
          type="text"
          placeholder="Write Name"
          onChange={this.writeValue}
        />
        <Input
          type="text"
          placeholder="Title Name"
          onChange={this.titleValue}
        />
        <Input
          type="text"
          placeholder="Publisher Name"
          onChange={this.publisherValue}
        />
        <button
          type="submit"
          className="btn btn-success"
          onClick={() =>
            this.props.postItem(
              this.state.write,
              this.state.title,
              this.state.publisher
            )
          }
        >
          Create
        </button>
      </div>
    );
  }
}
