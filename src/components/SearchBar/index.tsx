import * as React from "react";

import { SearchBoxProps } from "./interface";
import { Input } from "../Input";

export default class SearchBar extends React.Component<SearchBoxProps, {}> {
  constructor(props: any) {
    super(props);
    this.handleFilterTextChange = this.handleFilterTextChange.bind(this);
  }

  handleFilterTextChange(e: any) {
    this.props.onFilterTextChange(e.target.value);
  }

  render() {
    return (
      <form>
        <Input
          type="text"
          placeholder="Search..."
          value={this.props.filterText}
          onChange={this.handleFilterTextChange}
        />
      </form>
    );
  }
}
