import * as React from "react";

import { DataListProps, Book } from "./interface";

export class DataList extends React.Component<DataListProps> {
  constructor(props: any) {
    super(props);
  }

  public render() {
    const filterText = this.props.filterText;
    return (
      <table className="table">
        <thead>
          <tr>
            {this.props.itemList.length > 0 &&
              Object.keys(this.props.itemList[0]).map(
                (keyItem: keyof Book, keyIndex) => {
                  return <td key={keyIndex}>{keyItem}</td>;
                }
              )}
            <th>Delete</th>
            <th>Detail</th>
          </tr>
        </thead>
        <tbody>
          {this.props.itemList
            .filter((item: Book) => {
              if (
                item.title.indexOf(filterText) != -1 ||
                item.writer.indexOf(filterText) != -1
              ) {
                return item;
              }
            })
            .map((item: Book, index) => {
              return (
                <tr key={index}>
                  {Object.keys(item).map((keyItem: keyof Book, keyIndex) => {
                    return <td key={keyIndex}>{item[keyItem]}</td>;
                  })}
                  <td>
                    <button className="btn btn-danger" onClick={() => this.props.deleteItem(item.id)}>
                      Delete
                    </button>
                  </td>
                  <td>
                    <button className="btn btn-info" onClick={() => this.props.detailItem(item.id)}>
                      Detail
                    </button>
                  </td>
                </tr>
              );
            })}
        </tbody>
      </table>
    );
  }
}
