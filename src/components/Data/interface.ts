export interface DataListProps {
  itemList: Book[];
  filterText?: string;
  deleteItem?: any;
  detailItem?: any;
}

export interface Book {
  id: number;
  publisher: string;
  title: string;
  writer: string;
}

