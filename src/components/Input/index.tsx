import * as React from "react";

import { InputProps } from "./interface";

export class Input extends React.Component<InputProps, {}> {
  public render() {
    return (
      <input
        type={this.props.type}
        className="form-control"
        placeholder={this.props.placeholder}
        value={this.props.value}
        onChange={this.props.onChange}
      />
    );
  }
}
