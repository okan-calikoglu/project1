export interface InputProps {
  placeholder?: string,
  type: string,
  value?: any,
  onChange?: any;
}