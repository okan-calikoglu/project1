import * as React from "react";
import { DataListProps } from "../Data/interface";
import { FilterableTableState, FilterableTableProps } from "./interface";

import SearchBar from "../SearchBar";
import { DataList } from "../Data";

export default class FilterableTable extends React.Component<
  FilterableTableProps & DataListProps,
  FilterableTableState
> {
  constructor(props: any) {
    super(props);
    this.state = {
      filterText: ""
    };
    this.handleFilterTextChange = this.handleFilterTextChange.bind(this);
  }

  handleFilterTextChange(filterText: any) {
    this.setState({
      filterText: filterText
    });
  }

  render() {
    return (
      <div className="search">
        <SearchBar
          filterText={this.state.filterText}
          onFilterTextChange={this.handleFilterTextChange}
        />
        <DataList
          deleteItem={this.props.deleteItem}
          detailItem={this.props.detailItem}
          itemList={this.props.itemList}
          filterText={this.state.filterText}
        />
      </div>
    );
  }
}
