export interface FilterableTableState {
  filterText?: string,
}

export interface FilterableTableProps {
  itemList: Book[];
}

export interface Book {
  id: number;
  publisher: string;
  title: string;
  writer: string;
}

