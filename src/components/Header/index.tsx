import * as React from "react";
import { Link } from "react-router-dom";

const logo = require("./logo.svg");

const Header = () => {
  return (
    <header>
      <div className="logo">
        <img src={logo} alt="logo" />
      </div>
      <div>
        <Link to="/create">Create</Link>
      </div>
    </header>
  );
};

export default Header;
