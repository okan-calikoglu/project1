import { Book } from "./components/Data/interface";
import { CreateItemState } from "./components/CreateItem/interface";

export interface AppStateProps {
  itemList: Book[];
}

export interface AppState {
  state: CreateItemState;
}