import * as React from "react";

import axios from "axios";
import { AppStateProps } from "../../interface";
import { Book } from "../../components/Data/interface";

import FilterableTable from "../../components/FilterableTable";
import Header from "../../components/Header";

class Home extends React.Component<{}, AppStateProps> {
  constructor(props: any) {
    super(props);
    this.state = {
      itemList: []
    };
    this.deletePost = this.deletePost.bind(this);
    this.detailPost = this.detailPost.bind(this);
  }
  componentDidMount() {
    this.getItem();
  }

  getItem() {
    axios.get("http://5bace0a3092a7800144a2224.mockapi.io/books").then(res => {
      this.setState({
        itemList: res.data
      });
    });
  }

  deletePost(id: number) {
    axios
      .delete(`http://5bace0a3092a7800144a2224.mockapi.io/books/${id}`)
      .then(res => {
        var cloneState: Book[] = [...this.state.itemList];
        var result: Book[] = cloneState.filter((word: Book) => word.id != id);
        // for (var i = 0; i < cloneState.length; i++) {
        //   if (cloneState[i]["id"] !== id) {
        //     cloneState.push(cloneState[i]);
        //   }
        // }

        this.setState({
          itemList: result
        });
      })

      .catch(err => {
        console.log(err);
      });
  }

  // postItem = (write: AppState, title: AppState, publisher: AppState) => {
  //   axios
  //     .post("http://5bace0a3092a7800144a2224.mockapi.io/books/", {
  //       writer: write,
  //       title: title,
  //       publisher: publisher
  //     })
  //     .then(function(response) {
  //       alert("Veri İletildi ");
  //     })
  //     .catch(function(error) {
  //       console.log(error);
  //     });
  // };

  detailPost(id: number) {
    location.href = `/detail/${id}`;
  }

  public render() {
    return (
      <div>
        <div className="container">
          <Header />
          <FilterableTable
            itemList={this.state.itemList}
            deleteItem={this.deletePost}
            detailItem={this.detailPost}
          />
          {/* <CreateItem postItem={this.postItem} /> */}
        </div>
      </div>
    );
  }
}

export default Home;
