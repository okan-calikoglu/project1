import * as React from "react";
import axios from "axios";
const arrowIcon = require("../Detail/icon.svg"); 

import { AppState } from "../../interface";
import CreateItem from "../../components/CreateItem";

class CreatePage extends React.Component<{}> {
  constructor(props: any) {
    super(props);
  }

  postItem = (write: AppState, title: AppState, publisher: AppState) => {
    axios
      .post("http://5bace0a3092a7800144a2224.mockapi.io/books/", {
        writer: write,
        title: title,
        publisher: publisher
      })
      .then(function(response) {
        alert(
          `"Data: \n Writer: ${response.data.writer},\n Title: ${
            response.data.title
          },\n Publisher: ${response.data.publisher} "`
        );
        location.href = "/";
      })
      .catch(function(error) {
        console.log(error);
      });
  };

  public render() {
    return (
      <div className="container">
        <a className="btn btn-link" href="/" style={{ position: "absolute", top: "30px", left: "0" }}>
          <span className="back-icon">
            <img src={arrowIcon} alt="Back Icon" />
          </span>
        </a>
        <CreateItem postItem={this.postItem} />
      </div>
    );
  }
}

export default CreatePage;
