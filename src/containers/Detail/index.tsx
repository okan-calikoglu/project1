import * as React from "react";
import axios from "axios";

const arrowIcon = require("./icon.svg"); 

class Detail extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      detail: ""
    };
  }

  componentDidMount() {
    axios
      .get(
        `http://5bace0a3092a7800144a2224.mockapi.io/books/${
          this.props.match.params.id
        }`
      )
      .then(res => {
        this.setState({
          detail: res.data
        });
      });
  }

  public render() {
    return (
       <div
        className="container"
        style={{ paddingTop: "100px", paddingLeft: "40px" }}
      >
        <a href="/" style={{ position: "absolute", top: "30px", left: "40px" }}>
          <span className="back-icon">
            <img src={arrowIcon} alt="Back Icon" />
          </span>
        </a>
        <h1>
          ID:
          {this.state.detail["id"]}
        </h1>
        <div>Writer Name: {this.state.detail["writer"]}</div>
        <div>Title Name: {this.state.detail["title"]}</div>
        <div>Publisher: {this.state.detail["publisher"]}</div>
      </div>
    );
  }
}

export default Detail;
